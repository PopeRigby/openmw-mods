# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import stat

from pybuild import Pybuild2
from pybuild.info import PV, P


class Package(Pybuild2):
    NAME = "Groundcoverify!"
    DESC = "a script using DeltaPlugin to turn regular groundcover into openmw-style groundcover"
    HOMEPAGE = "https://gitlab.com/bmwinger/groundcoverify"
    SRC_URI = f"https://gitlab.com/bmwinger/groundcoverify/-/archive/{PV}/{P}.tar.gz"
    RDEPEND = ">=bin/delta-plugin-0.19"
    KEYWORDS = "openmw"
    S = f"{P}/{P}"

    def src_install(self):
        os.makedirs(os.path.join(self.D, "bin"))
        os.makedirs(os.path.join(self.D, "etc"))
        exe_name = "groundcoverify.py"
        os.chmod(
            exe_name,
            os.stat(exe_name).st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH,
        )
        path = os.path.join(self.D, "bin", exe_name)
        os.rename(exe_name, path)
        os.rename(
            "groundcoverify.toml", os.path.join(self.D, "etc", "groundcoverify.toml")
        )
