# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Glowing Flames"
    DESC = "Flames are now glow mapped and/or properly illuminated."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46124"
    KEYWORDS = "openmw"
    LICENSE = "mw-nc-sa"
    NEXUS_URL = HOMEPAGE
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    TEXTURE_SIZES = "64"
    SRC_URI = """
        Glowing_Flames-46124-2-3-1560337896.zip
        NoMoreLightlessFlames-46124-1-1-1544995104.zip
    """
    IUSE = "+tweaks"
    # Note: If true-lights-and-darkness is updated, new names that the plugin
    # goes by will need to be noted in these overrides
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("Glowing Flames - NoMoreLightlessFlames v1.1.ESP"),
                File(
                    "Glowing Flames - TrueLightsAndDarkness Tweaks.ESP",
                    OVERRIDES=[
                        "Glowing Flames - NoMoreLightlessFlames v1.1.ESP",
                        "True_Lights_And_Darkness_1.1.esp",
                        "True_Lights_And_Darkness_1.0-NoDaylight.esp",
                    ],
                    REQUIRED_USE="tweaks",
                ),
            ],
            S="Glowing_Flames-46124-2-3-1560337896",
        ),
        InstallDir(
            "NoMoreLightlessFlames v1.1/Data Files",
            PLUGINS=[
                File(
                    "Glowing Flames - NoMoreLightlessFlames v1.1.ESP",
                    OVERRIDES=[
                        "True_Lights_And_Darkness_1.1.esp",
                        "True_Lights_And_Darkness_1.0-NoDaylight.esp",
                    ],
                )
            ],
            S="NoMoreLightlessFlames-46124-1-1-1544995104",
        ),
    ]
