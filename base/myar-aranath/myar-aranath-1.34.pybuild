# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import json
import os
import re
import shutil
from glob import glob

from chardet import detect

from common.mw import DOWNLOAD_DIR, MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Myar Aranath"
    DESC = "A total conversion mod set on the continent of Myar"
    HOMEPAGE = """
        https://sureai.net/games/myararanath/
        https://wiki.sureai.net/Myar_Aranath:_Relict_of_Kallidar
    """
    KEYWORDS = "~openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = ""
    SRC_URI = """
        http://dl1.sureai.net/files/pro/myararanath/dow/de/MA_Setup.zip
        http://dl1.sureai.net/files/pro/myararanath/dow/de/MA_PATCH_1.31.exe
        l10n_de? (
            https://forum.sureai.net/download/file.php?id=6093 -> MA_Patch_1.00_to_1.34_non-MAE.esp
        )
        l10n_en? (
            MA_Translations.7z
        )
    """
    NEXUS_SRC_URI = """
        l10n_en? (
            https://www.nexusmods.com/morrowind/mods/49823?tab=files&file_id=1000024862
            -> Kezyma's_Myar_Aranath_English_Translations_Race_Fix-49823-0-1-1621537341.zip
        )
    """
    DEPEND = "bin/7z"
    RDEPEND = """
        || (
            =base/morrowind-1.6.1820-r1[tribunal,bloodmoon,-plugins]
            base/morrowind-data[tribunal,bloodmoon]
        )
        !!=base/morrowind-1.6.1820-r2
        !!=base/morrowind-1.6.1820-r1[plugins]
    """
    IUSE = "l10n_en l10n_de"
    REQUIRED_USE = "^^ ( l10n_en l10n_de )"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            ARCHIVES=[File("marok.bsa")],
            PLUGINS=[
                File("MA_RoK_1.00.ESM", REQUIRED_USE="l10n_de"),
                File("ma_script_base.ESM", REQUIRED_USE="l10n_de"),
            ],
            # Made obsolete by the later patches
            BLACKLIST=["MA_Patch_1.0to1.1.exp", "MA_Patch_1.1.2.exp"],
            S="MA_Setup",
        ),
        InstallDir(
            "Data Files",
            S="MA_PATCH_1.31",
        ),
        InstallDir(
            ".",
            S=".",
            PLUGINS=[File("MA_Patch_1.00_to_1.34_non-MAE.esp")],
            WHITELIST=["MA_Patch_1.00_to_1.34_non-MAE.esp"],
            REQUIRED_USE="l10n_de",
        ),
        # Note: The English version may not contain all the changes from the patches
        # as it was created prior to the later ones.
        InstallDir(
            "Morrowind/Data Files",
            S="MA_RoK_Eng_v0001",
            PLUGINS=[File("MA_RoK_Eng v0001.esm")],
            REQUIRED_USE="l10n_en",
        ),
        InstallDir(
            ".",
            S="MA_AdditionalTranslations",
            PLUGINS=[File("Additional_Journal_Translations_ENG.esp")],
            REQUIRED_USE="l10n_en",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Myar_Aranath_English_Race_Fix.ESP")],
            S="Kezyma's_Myar_Aranath_English_Translations_Race_Fix-49823-0-1-1621537341",
            REQUIRED_USE="l10n_en",
        ),
    ]

    def pkg_nofetch(self):
        super().pkg_nofetch()
        if "l10n_en" in self.USE:
            print(
                "Please download the following file into one of the following download directories:"
            )
            print("    " + DOWNLOAD_DIR)
            print("    " + os.path.expanduser("~/Downloads"))
            print()
            print("https://www.moddb.com/downloads/start/52209")

    def src_unpack(self):
        os.chdir(self.WORKDIR)
        self.unpack(os.path.join(DOWNLOAD_DIR, "MA_Setup.zip"))
        self.execute(
            f'7z x -oMA_Setup -- {os.path.join("MA_Setup", "Myar_Aranath_Setup.exe")}'
        )
        self.execute(
            f'7z x -oMA_PATCH_1.31 -- {os.path.join(DOWNLOAD_DIR, "MA_PATCH_1.31.exe")}'
        )
        if "l10n_de" in self.USE:
            shutil.copy2(
                os.path.join(DOWNLOAD_DIR, "MA_Patch_1.00_to_1.34_non-MAE.esp"),
                "MA_Patch_1.00_to_1.34_non-MAE.esp",
            )
        elif "l10n_en" in self.USE:
            self.unpack(os.path.join(DOWNLOAD_DIR, "MA_Translations.7z"))
            os.chdir("MA_Translations")
            self.unpack("MA_RoK_Eng_v0001.7z")
            self.unpack("MA_AdditionalTranslations.7z")
            self.unpack(
                os.path.join(
                    DOWNLOAD_DIR,
                    "Kezyma's_Myar_Aranath_English_Translations_Race_Fix-49823-0-1-1621537341.zip",
                )
            )

    def read_ini(self, path):
        section = None
        self.FALLBACK = {}
        with open(os.path.join(self.FILESDIR, "ini_whitelist.json"), "r") as file:
            whitelist = set(json.load(file))

        with open(path, "rb") as file:
            result = detect(file.read())

        if result["confidence"] < 1.0:
            self.warn(
                f'Detected {result["encoding"]} for Morrowind.ini with confidence of '
                f'only {result["confidence"]}\n'
                "You should double-check that this encoding is correct and that the "
                "fallback entries in openmw.cfg are not garbled."
            )

        with open(path, "r", encoding=result["encoding"]) as config:
            for line in config.readlines():
                if re.match(r"\s*^\[.*\]\s*$", line):
                    section = line.strip().strip("[]")
                    self.FALLBACK[section] = {}
                elif "=" in line:
                    key = line.split("=", 1)[0].strip()
                    value = line.split("=", 1)[1].strip()
                    if f"{section}:{key}" in whitelist:
                        self.FALLBACK[section][key] = value

    def src_prepare(self):
        if "l10n_en" in self.USE:
            self.read_ini(
                os.path.join(self.WORKDIR, "MA_RoK_Eng_v0001/Morrowind/Morrowind.ini")
            )
        else:
            self.read_ini("Morrowind.ini")

    def src_install(self):
        self.dodoc(os.path.join(self.WORKDIR, "MA_Setup", "Readme.txt"))
        super().src_install()

        # Fix directories of files in the 1.31 patch
        os.chdir(os.path.join(self.D, "pkg", self.CATEGORY, self.PN))
        os.makedirs("meshes/bb")
        os.makedirs("textures")
        for file in glob("bb_*.nif"):
            os.rename(file, os.path.join("meshes", "bb", file))

        for file in glob("*.dds"):
            os.rename(file, os.path.join("textures", file))

        os.makedirs("sound/MWE")
        for file in glob("MWE_*.wav"):
            os.rename(file, os.path.join("sound", "MWE", file))

        os.makedirs("sound/fx/em")
        for file in glob("*.WAV"):
            os.rename(file, os.path.join("sound", "fx", "em", file))
        os.rename("BRID_01.wav", os.path.join("sound", "fx", "em", "BRID_01.wav"))

        os.makedirs("splash")
        for file in glob("Splash_*.tga"):
            os.rename(file, os.path.join("splash", file))

        os.makedirs("meshes/c")
        for file in glob("zing_*.nif"):
            os.rename(file, os.path.join("meshes", "c", file))

        os.makedirs("icons/c")
        for file in glob("TX_c_zing_*.tga"):
            newfile = file.replace("zing", "ring")
            os.rename(file, os.path.join("meshes", "c", newfile))

        # Assuming remaining mp3 files are music.
        # No idea which directories they were supposed to be in, so lets put them
        # in all three for now
        os.makedirs("music/battle")
        os.makedirs("music/explore")
        os.makedirs("music/special")
        for file in glob("*.mp3"):
            shutil.copy2(file, os.path.join("music", "battle", file))
            shutil.copy2(file, os.path.join("music", "explore", file))
            os.rename(file, os.path.join("music", "special", file))

        os.makedirs("video")
        for file in glob("*.bik"):
            os.rename(file, os.path.join("video", file))

        os.makedirs("icons/custompotions2")
        with open(os.path.join(self.FILESDIR, "custompotions2.txt")) as file:
            for line in file.readlines():
                line = line.strip()
                os.rename(line, os.path.join("icons", "custompotions2", line))
