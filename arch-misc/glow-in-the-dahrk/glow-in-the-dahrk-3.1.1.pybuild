# Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Glow in the Dahrk"
    DESC = "Windows transition to glowing versions at night"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45886"
    # From README (paraphrased): Assets may be used, but you may not rehost this
    # unless I have been unable to be reached for 3 months.
    LICENSE = "free-derivation"
    RDEPEND = """
        base/morrowind
        dark-molag-mar? ( arch-towns/dark-molag-mar )
    """
    KEYWORDS = "openmw"
    MAIN_FILE = "Glow_in_the_Dahrk-45886-3-1-1-1655254988"
    SRC_URI = f"""
        {MAIN_FILE}.7z
        devtools? ( Mesh_Converter_Scripts_and_Instructions-45886-1-0.7z )
    """
    # TODO: Patches are available for:
    # - Windoors
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45886"
    TEXTURE_SIZES = "512 1024"
    # TODO: DATA_OVERRIDES = "distant-lights"
    IUSE = "+sunrays telvanni-dormers glass devtools dark-molag-mar"
    # Since 3.0 there is no OpenMW-compatible option to disable sunrays
    REQUIRED_USE = "sunrays"
    # Technically only the patch needs to override
    DATA_OVERRIDES = "arch-towns/dark-molag-mar"
    INSTALL_DIRS = [
        InstallDir("00 Core", BLACKLIST=["MWSE"], S=MAIN_FILE),
        InstallDir(
            "01 Hi Res Window Texture Replacer",
            S=MAIN_FILE,
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "02 Nord Glass Windows",
            S=MAIN_FILE,
            REQUIRED_USE="glass",
        ),
        InstallDir(
            "03 Telvanni Dormers on Vvardenfell",
            PLUGINS=[
                File("GITD_Telvanni_Dormers_NoUvirith.ESP"),
                File("GITD_Telvanni_Dormers.ESP"),
            ],
            S=MAIN_FILE,
            REQUIRED_USE="telvanni-dormers",
        ),
        InstallDir(
            "04 Raven Rock Glass Windows",
            PLUGINS=[File("GITD_WL_RR_Interiors.esp")],
            S=MAIN_FILE,
            REQUIRED_USE="glass",
        ),
        InstallDir(
            "05 Dark Molag Mar Patch",
            S=MAIN_FILE,
            REQUIRED_USE="dark-molag-mar",
        ),
        # InstallDir(
        #     "06 Windoors Patch",
        #     PLUGINS=[File("RR_Windoors_Glow_1C.esp")],
        #     S=MAIN_FILE,
        #     REQUIRED_USE="windoors",
        # ),
        InstallDir(
            ".",
            S="Mesh_Converter_Scripts_and_Instructions-45886-1-0",
            REQUIRED_USE="devtools",
        ),
    ]
