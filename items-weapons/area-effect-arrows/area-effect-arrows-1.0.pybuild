# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir, apply_patch


class Package(MW):
    NAME = "Area Effect Arrows"
    DESC = "Adds a new shop selling a wide selection of quality Marksman weapons"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    IUSE = "minimal"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/area_effect_arrows.zip
        https://gitlab.com/bmwinger/umopp/uploads/f84b60994bea77b80f07a2e09fde7f84/areaeffectarrows-umopp-3.0.2.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("AreaEffectArrows.esp")], S="area_effect_arrows")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "areaeffectarrows-umopp-3.0.2")
        if "minimal" in self.IUSE:
            apply_patch(os.path.join(path, "AreaEffectArrows_compat_plugin.patch"))
        else:
            apply_patch(os.path.join(path, "AreaEffectArrows_plugin.patch"))
